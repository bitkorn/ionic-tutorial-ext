# extended Ionic Tutorial project
This repository is the content of an Ionic Tutorial project. Take a look to the [docs](https://ionicframework.com/docs/intro/tutorial/).

Creates with:

    ionic start MyIonicTutorial tutorial

## extended with
Things with which I have extended the tutorial project
### BarcodeTake
The first thing that was added is the page "BarcodeTake". It takes a barcode with ```@ionic-native/barcode-scanner``` and display his content.

Additional navigation item is also added :)