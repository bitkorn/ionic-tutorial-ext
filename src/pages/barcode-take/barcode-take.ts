import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@Component({
  selector: 'page-barcode-take',
  templateUrl: 'barcode-take.html'
})
export class BarcodeTakePage {
  barcodeData: any;
  constructor(private barcodeScanner: BarcodeScanner) {

  }

  takeBarcode(event) {
    this.barcodeScanner.scan().then(barcodeData => {
      // this.barcodeData = JSON.stringify(barcodeData).replace(/(?:\r\n|\r|\n)/g, '<br>');
      this.barcodeData = barcodeData.text.replace(/(?:\r\n|\r|\n)/g, '<br>');
      console.log('Barcode data', barcodeData);
     }).catch(err => {
         console.log('Error', err);
     });
  }
}
